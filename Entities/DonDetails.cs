﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class DonDetails : Don
    {
        #region ATTRIBUTES
        public List<Horaire>? HorairesRelais { get; set; }
        public List<Horaire>? HorairesDon { get; set; }
        public string? ProduitName { get; set; }
        public string? DonorFirstName { get; set; }  
        public string? DonorLastName { get; set; }

        public string? BeneficiaryFirstName { get; set; }
        public string? BeneficiaryLastName { get; set; }
        public string? UniteName { get; set; }
        public string? DonPostcode { get; set; }
        public string? DonCity { get; set; }
        public string? RelaisPostcode { get; set; }
        public string? RelaisCity { get; set; }
        public string? RelCompAdr { get; set; }
        public string? RelStreetName { get; set; }
        public int? RelStreetNb { get; set; }
        public int? PartStreetNb { get; set; }
        public string? PartStreetName { get; set; }
        public string? PartCompAdr { get; set; }
        public string? PartCity { get; set; }
        public string? PartPostcode { get; set; }
        public string? DonorPostcode { get; set; }
        public string? DonorCity { get; set; }
        public string? DonorCompAdr { get; set; }
        public string? DonorStreetName { get; set; }
        public int? DonorStreetNb { get; set; }
        public string? DonorTel { get; set; }
        public string? DonorEmail { get; set; }

        public string? BeneficiaryTel { get; set; }
        public string? BeneficiaryEmail { get; set; }
        public string? CatName { get; set; }
        public string? Cat2Name { get; set; }
        public string? Cat3Name { get; set; }

        public int? CatId { get; set; }
        public int? Cat2Id { get; set; }
        public int? Cat3Id { get; set; }


        //public int DonorRating { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DonDetails()
        {
        }
        public DonDetails(Don don)
        {
            Id = don.Id;
            Qty = don.Qty;
            Dlc = don.Dlc;
            Ddm = don.Ddm;
            CreateDate = don.CreateDate;
            ReserveDate = don.ReserveDate;
            CancelDate = don.CancelDate;
            Picture = don.Picture;
            AltStreetNb = don.AltStreetNb;
            AltStreetName = don.AltStreetName;
            AltCompAdr = don.AltCompAdr;
            AvailableCollect = don.AvailableCollect;
            Receive = don.Receive;
            NoReceive = don.NoReceive;
            Handover = don.Handover;
            NoHandover = don.NoHandover;
            FinalDate = don.FinalDate;
            BeneficiaryId = don.BeneficiaryId;
            StockageId = don.StockageId;
            ProduitId = don.ProduitId;
            DonorId = don.DonorId;
            UniteId = don.UniteId;
            ConservationId = don.ConservationId;
            LocalisationId = don.LocalisationId;
        }
        #endregion
    }
}
