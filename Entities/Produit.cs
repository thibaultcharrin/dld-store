﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Produit
    {
        #region ATTRIBUTES
        public int? Id { get; set; }
        public int? Cat3Id { get; set; }
        public int? ConservationId { get; set; }
        public string? Name { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Produit()
        { 
        }

        public Produit(int? id, int? cat3Id, int? conservationId, string? name)
        {
            Id = id;
            Cat3Id = cat3Id;
            ConservationId = conservationId;
            Name = name;
        }
        #endregion
    }
}
