﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Carnet
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
        public int? NbCred { get; set; }
        public float? Price { get; set; }

        public Carnet()
        {
            Id = Id;
            Name = Name;
            this.NbCred = NbCred;
            Price = Price;
        }

        public Carnet(int? id, string? name, int? nbCred, float? price)
        {
            Id = id;
            Name = name;
            NbCred = nbCred;
            Price = price;
        }
    }
}
