﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Adherent
    {
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public int? LocalisationId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public DateTime? Dob { get; set; }
        public string? Email { get; set; }
        public string? Pw { get; set; }
        public int? StreetNb { get; set; }
        public string? StreetName { get; set; }
        public string? CompAdr { get; set; }
        public string? Tel { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public DateTime? DeactivateDate { get; set; }
        public int? ActivationCode { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Adherent()
        {
        }

        public Adherent(int? id, int? localisationId, string? firstName,
            string? lastName, DateTime? dob, string? email, string? pw,
            int? streetNb, string? streetName, string? compAdr, string? tel,
            DateTime? createDate, DateTime? confirmDate, DateTime? deactivateDate,
            int? activationCode)
        {
            Id = id;
            LocalisationId = localisationId;
            FirstName = firstName;
            LastName = lastName;
            Dob = dob;
            Email = email;
            Pw = pw;
            StreetNb = streetNb;
            StreetName = streetName;
            CompAdr = compAdr;
            Tel = tel;
            CreateDate = createDate;
            ConfirmDate = confirmDate;
            DeactivateDate = deactivateDate;
            ActivationCode = activationCode;
        }


        #endregion
    }
}
