﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Distance
    {
        public int? Id { get; set; }
        public int? LocalisationId { get; set; }
        public int? AvgDistance { get; set; }

        public Distance()
        {
        }

        public Distance(int? id, int? localisationId, int? avgDistance)
        {
            Id = id;
            LocalisationId = localisationId;
            AvgDistance = avgDistance;
        }
    }
}
