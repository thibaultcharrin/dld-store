﻿using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Models
{
    public class DonSearchViewModel
    {
        public int Id { get; set; } = 0;
        public int Rayon { get; set; } = 0;
        public int Categorie { get; set; } = 0;
        public int SousCategorie { get; set; } = 0;
        public int Produit { get; set; } = 0;

        //FRONT
        public string RayonSelector { get; set; } = "FeL";


        public DonMultiSearch? SearchCriteria { get; set; }
        public List<DonDetails> Dons { get; set; }
        public List<Categorie> Categories { get; set; }
        public List<Categorie2> Categories2 { get; set; }
        public List<Categorie3> Categories3 { get; set; }
        public List<ProduitDetails> Produits { get; set; }

        //constructeur vide pour retourner tout
        public DonSearchViewModel()
        {
            SearchCriteria = new DonMultiSearch();
            Dons = new List<DonDetails>();
        }
    }
}