﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class ConservationDao : DAO
    {
        public List<Conservation> GetAllConservations()
        {

            List<Conservation> result = new List<Conservation>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM Conservation";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Conservation Conservation = DataReaderToEntity(dr);

                    result.Add(Conservation);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }

        private Conservation DataReaderToEntity(DbDataReader dr)
        {
            Conservation Conservation = new Conservation();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                Conservation.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                Conservation.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return Conservation;
        }
    }
}