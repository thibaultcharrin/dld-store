-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: projet2_dld
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `accessibilite`
--

LOCK TABLES `accessibilite` WRITE;
/*!40000 ALTER TABLE `accessibilite` DISABLE KEYS */;
INSERT INTO `accessibilite` VALUES (1,'commerçant'),(2,'pick-up box'),(3,'donateur');
/*!40000 ALTER TABLE `accessibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `achat`
--

LOCK TABLES `achat` WRITE;
/*!40000 ALTER TABLE `achat` DISABLE KEYS */;
/*!40000 ALTER TABLE `achat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `adherent`
--

LOCK TABLES `adherent` WRITE;
/*!40000 ALTER TABLE `adherent` DISABLE KEYS */;
INSERT INTO `adherent` VALUES (1,1,'Willy','Lepante','1989-12-12 00:00:00','willy.lepante@gmail.com','willylepante',13,'rue du Bac','à lauréat','0605040302','2021-12-12 21:52:58','2021-12-12 21:52:58',NULL,1234),(2,1,'Billy','Etnapel','1989-12-13 00:00:00','billy.etnapel@gmail.com','billyetnapel',14,'rue du Cab',NULL,'0666666666','2021-12-13 21:52:58','2021-12-13 21:55:58',NULL,6666),(3,1,'Ylly','Lepapel','1998-12-12 00:00:00','yllylly@gmail.com','yllylly',16,'rue du rue','du rue','0606060606','2021-12-12 12:12:12','2021-12-13 13:13:13',NULL,1111);
/*!40000 ALTER TABLE `adherent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ca_mesurer`
--

LOCK TABLES `ca_mesurer` WRITE;
/*!40000 ALTER TABLE `ca_mesurer` DISABLE KEYS */;
INSERT INTO `ca_mesurer` VALUES (1,1),(1,2),(2,2),(2,3),(3,3),(3,4);
/*!40000 ALTER TABLE `ca_mesurer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `carnet`
--

LOCK TABLES `carnet` WRITE;
/*!40000 ALTER TABLE `carnet` DISABLE KEYS */;
INSERT INTO `carnet` VALUES (1,'à l\'unité',1,1),(2,'carnet de 5',5,4),(3,'carnet de 10',10,7);
/*!40000 ALTER TABLE `carnet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cat`
--

LOCK TABLES `cat` WRITE;
/*!40000 ALTER TABLE `cat` DISABLE KEYS */;
INSERT INTO `cat` VALUES (1,'Fruits et Légumes'),(2,'Viandes et Poissons'),(3,'Pains et Pâtisseries'),(4,'Frais'),(5,'Surgelés'),(6,'Boissons'),(7,'Epicerie salée'),(8,'Epicerie sucrée');
/*!40000 ALTER TABLE `cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cat2`
--

LOCK TABLES `cat2` WRITE;
/*!40000 ALTER TABLE `cat2` DISABLE KEYS */;
INSERT INTO `cat2` VALUES (1,1,'Fruits'),(2,1,'Légumes'),(3,1,'Jus de fruits et Légumes frais'),(4,1,'Fruits et Légumes secs'),(5,2,'Boucherie'),(6,2,'Volaille et Rôtisserie'),(7,2,'Poissonnerie'),(8,2,'Traîteur de la mer'),(9,3,'Bûches et Pâtisseries'),(10,3,'Viennoiseries et Brioches fraîches'),(11,3,'Pains frais'),(12,3,'Toasts et Pains de mie'),(13,3,'Pains burger et précuits'),(14,4,'Foie gras et Charcuterie'),(15,4,'Fromages à la coupe, à déguster'),(16,4,'Fromages pour raclette et apéritifs'),(17,4,'Crèmerie'),(18,4,'Yaourts, Compotes et Desserts'),(19,4,'Plats cuisinés, Snacking, Salades'),(20,4,'Végétarien et Végétal'),(21,5,'Repas de fête: Bûches'),(22,5,'Apéritifs, Entrées et Snacking'),(23,5,'Poissons et Fruits de mer'),(24,5,'Viandes'),(25,5,'Frites et Pommes de terre'),(26,5,'Bûches, Glaces et Sorbets'),(27,5,'Pain, Pâtisseries et Viennoiseries'),(28,5,'Légumes et Fruits'),(29,5,'Plats cuisinés'),(30,5,'Pizza, Quiches et Tartes'),(31,6,'Champagnes et Pétillants'),(32,6,'Cave à vins'),(33,6,'Apéritifs et Spiritueux'),(34,6,'Bières et Cidres'),(35,6,'Eaux'),(36,6,'Colas, Thés glacés et Sodas'),(37,6,'Jus de fruits et Légumes'),(38,6,'Laits'),(39,7,'Pour l\'apéritif'),(40,7,'Foie gras et Conserves'),(41,7,'Soupes et Croutons'),(42,7,'Les plats cuisinés'),(43,7,'Pâtes, Riz, Purées et Féculents'),(44,7,'Huiles, Vinaigres et Vinaigrettes'),(45,7,'Sauces et Condiments'),(46,7,'Sel, Epices et Bouillons'),(47,8,'Chocolats de Noël'),(48,8,'Café, Thé et Boissons chaudes'),(49,8,'Petit déjeuner'),(50,8,'Biscuits et Gâteaux'),(51,8,'Chocolats et Bonbons'),(52,8,'Sucres, Farines, Coulis et Préparations gâteaux'),(53,8,'Compotes, Fruits au sirop et Crêmes desserts');
/*!40000 ALTER TABLE `cat2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cat3`
--

LOCK TABLES `cat3` WRITE;
/*!40000 ALTER TABLE `cat3` DISABLE KEYS */;
INSERT INTO `cat3` VALUES (1,1,'Agrumes'),(2,1,'Pommes, Poires et Raisins'),(3,1,'Bananes et Kiwis'),(4,1,'Prunes, Abricots, Pêches'),(5,1,'Fruits rouges'),(6,1,'Melons et Pastêques'),(7,1,'Fruits exotiques'),(8,2,'Potirons, Courges et Légumes anciens'),(9,2,'Carottes et Pommes de terre'),(10,2,'Choux, Poireaux, Navets et Céleris'),(11,2,'Légumes verts et Asperges'),(12,2,'Concombres, Avocats, Betteraves et Radis'),(13,2,'Salades et Tomates'),(14,2,'Aubergines, Courgettes et Poivrons'),(15,2,'Champignons'),(16,2,'Ail, Oignons et Echalotes'),(17,2,'Herbes aromatiques et Epices'),(18,3,'Jus de fruits'),(19,3,'Soupes fraîches et Gaspachos'),(20,3,'Smoothies'),(21,4,'Pruneaux et Fruits séchés'),(22,4,'Fruits secs aide cuilinaire'),(23,4,'Légumes secs'),(24,4,'Fruits secs mélanges et graînes'),(25,5,'Boeuf'),(26,5,'Viandes hachées'),(27,5,'Veau'),(28,5,'Porc'),(29,5,'Agneau'),(30,5,'Saucisses et Merguez'),(31,5,'Grillades et Brochettes'),(32,6,'Poulets, Pintades et Chapons'),(33,6,'Rôtisserie'),(34,6,'Dindes'),(35,6,'Canard'),(36,6,'Lapin'),(37,6,'Cordons bleus et Grignotes'),(38,6,'Foies et Gésiers'),(39,7,'Huîtres'),(40,7,'Coquillages et Crustacés'),(41,7,'Crevettes et Gambas'),(42,7,'Filets et pavés'),(43,7,'Saumons et Truites'),(44,8,'Saumons et Poissons fumés'),(45,8,'Caviar, Tarama et Tartinables'),(46,8,'Canapés et Blinis'),(47,8,'Sushis'),(48,8,'Poissons panés et cuisinés'),(49,8,'Surimis'),(50,8,'Soupes de poisson et Sauces'),(51,9,'Bûches de Noël et Pâtisseries festives'),(52,9,'Galettes des rois'),(53,9,'Gâteaux à partager'),(54,9,'Pâtisseries individuelles'),(55,9,'Tartes'),(56,9,'Macarons et Mignardises'),(57,9,'Cookies, beignets et muffins'),(58,10,'Crêpes'),(59,10,'Viennoiseries'),(60,10,'Brioches'),(61,11,'Baguettes'),(62,11,'Pains'),(63,11,'Pains spéciaux'),(64,12,'Toasts et canapés apéritifs'),(65,12,'Pains de mie nature'),(66,12,'Pains de mie complet et aux céréales'),(67,12,'Pains de mie sans croûte'),(68,12,'Pains sans gluten'),(69,13,'Pains burger'),(70,13,'Pains hot-dog et Panini'),(71,13,'Pains précuits');
/*!40000 ALTER TABLE `cat3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `conservation`
--

LOCK TABLES `conservation` WRITE;
/*!40000 ALTER TABLE `conservation` DISABLE KEYS */;
INSERT INTO `conservation` VALUES (1,'réfrigéré'),(2,'congelé'),(3,'surgelé'),(4,'temperature ambiante');
/*!40000 ALTER TABLE `conservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dimension`
--

LOCK TABLES `dimension` WRITE;
/*!40000 ALTER TABLE `dimension` DISABLE KEYS */;
/*!40000 ALTER TABLE `dimension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `distance`
--

LOCK TABLES `distance` WRITE;
/*!40000 ALTER TABLE `distance` DISABLE KEYS */;
/*!40000 ALTER TABLE `distance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `don`
--

LOCK TABLES `don` WRITE;
/*!40000 ALTER TABLE `don` DISABLE KEYS */;
INSERT INTO `don` VALUES (1,NULL,1,1,NULL,2,4,1,2,'2021-12-12 21:52:58',NULL,'2021-12-12 21:52:58',NULL,NULL,'orange.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,2,2,NULL,1,3,2,1,'2022-12-12 21:52:58',NULL,'2020-12-12 21:52:58',NULL,NULL,'citron.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,NULL,7,3,NULL,2,4,NULL,3,'2021-12-31 10:24:00',NULL,'2021-12-23 10:27:57',NULL,NULL,'bergamot.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,2,1,1,NULL,2,4,NULL,2,'2021-12-31 11:00:00',NULL,'2021-12-23 11:00:48','2021-12-23 12:07:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12-23 12:07:53',NULL),(6,2,1,1,NULL,2,4,NULL,2,'2021-12-31 11:00:00',NULL,'2021-12-23 11:01:35','2021-12-23 12:10:44',NULL,NULL,NULL,NULL,NULL,NULL,'2021-12-23 12:11:10',NULL,NULL,'2021-12-23 12:11:00',NULL),(7,NULL,1,1,NULL,2,4,NULL,2,'2021-12-31 11:00:00',NULL,'2021-12-23 11:02:31',NULL,'2021-12-23 13:34:55',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,NULL,1,1,NULL,1,1,NULL,2,'2022-01-13 11:06:00',NULL,'2021-12-23 11:06:09',NULL,'2021-12-23 14:08:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,1,1,NULL,1,1,NULL,25,'2021-12-24 11:09:00',NULL,'2021-12-23 11:09:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,NULL,1,1,NULL,1,1,NULL,123,'2021-12-25 11:10:00',NULL,'2021-12-23 11:10:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,4,1,NULL,1,1,NULL,456,NULL,NULL,'2021-12-23 11:14:20',NULL,NULL,'clementine.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,1,1,NULL,1,1,NULL,45,'2021-12-24 11:31:00',NULL,'2021-12-23 11:31:56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,NULL,3,3,NULL,2,4,NULL,32,'2021-12-31 11:35:00',NULL,'2021-12-23 11:35:32',NULL,NULL,'pamplemousse.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,NULL,5,3,NULL,2,4,NULL,35,'2021-12-31 13:36:00',NULL,'2021-12-23 13:37:13',NULL,NULL,'yuzu.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,NULL,11,3,NULL,6,1,NULL,1,'2021-12-31 14:22:00',NULL,'2021-12-23 14:22:24',NULL,NULL,'potiron.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,NULL,14,2,NULL,2,1,NULL,1,'2021-12-31 15:34:00',NULL,'2021-12-23 15:35:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,12,1,NULL,4,1,NULL,50,'2021-12-29 15:35:00',NULL,'2021-12-23 15:36:03',NULL,NULL,'jananas.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,13,2,NULL,1,4,NULL,500,'2021-12-28 15:36:00',NULL,'2021-12-23 15:37:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,NULL,9,3,NULL,2,4,NULL,5,'2021-12-27 15:37:00',NULL,'2021-12-23 15:38:05',NULL,NULL,'granny.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `horaire`
--

LOCK TABLES `horaire` WRITE;
/*!40000 ALTER TABLE `horaire` DISABLE KEYS */;
INSERT INTO `horaire` VALUES (11,1,1,NULL,'01:00:00','08:00:00','11:00:00','12:00:00'),(12,2,1,NULL,'02:00:00','09:00:00',NULL,NULL),(13,3,1,NULL,'03:00:00','10:00:00',NULL,NULL),(14,4,1,NULL,'04:00:00','11:00:00','12:00:00','15:00:00'),(15,5,1,NULL,'05:00:00','12:00:00','13:00:00','16:00:00'),(16,6,1,NULL,NULL,NULL,NULL,NULL),(17,7,1,NULL,NULL,NULL,NULL,NULL),(32,1,3,NULL,'12:00:00','20:00:00',NULL,NULL),(33,2,3,NULL,'12:00:00','20:00:00',NULL,NULL),(34,3,3,NULL,'12:00:00','20:00:00',NULL,NULL),(35,4,3,NULL,NULL,NULL,NULL,NULL),(36,5,3,NULL,NULL,NULL,NULL,NULL),(37,6,3,NULL,'09:00:00','12:00:00','14:00:00','18:00:00'),(38,7,3,NULL,'09:00:00','12:00:00','14:00:00','18:00:00');
/*!40000 ALTER TABLE `horaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `indisponibilite`
--

LOCK TABLES `indisponibilite` WRITE;
/*!40000 ALTER TABLE `indisponibilite` DISABLE KEYS */;
/*!40000 ALTER TABLE `indisponibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jour`
--

LOCK TABLES `jour` WRITE;
/*!40000 ALTER TABLE `jour` DISABLE KEYS */;
INSERT INTO `jour` VALUES (1,'lundi'),(2,'mardi'),(3,'mercredi'),(4,'jeudi'),(5,'vendredi'),(6,'samedi'),(7,'dimanche');
/*!40000 ALTER TABLE `jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `localisation`
--

LOCK TABLES `localisation` WRITE;
/*!40000 ALTER TABLE `localisation` DISABLE KEYS */;
INSERT INTO `localisation` VALUES (1,'Paris','75013'),(2,'Nantes','44200');
/*!40000 ALTER TABLE `localisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partenaire`
--

LOCK TABLES `partenaire` WRITE;
/*!40000 ALTER TABLE `partenaire` DISABLE KEYS */;
INSERT INTO `partenaire` VALUES (1,1,'EQL','eql@esl.com',NULL,'0123456789',6,'Montrouge','à côté du café','2021-12-12 21:52:58','2021-12-12 21:52:58',NULL,NULL);
/*!40000 ALTER TABLE `partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (1,1,4,'Orange'),(2,1,4,'Citron'),(3,1,4,'Pamplemousse'),(4,1,4,'Clémentine'),(5,1,4,'Yuzu'),(6,1,4,'Citron Vert'),(7,1,4,'Bergamotte'),(8,2,4,'Pomme Golden'),(9,2,4,'Pomme Granny'),(10,2,4,'Pomme Reinette'),(11,8,4,'Potiron'),(12,18,1,'Jus d\'ananas'),(13,21,4,'Abricot sec'),(14,25,1,'Entrecôte');
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `relais`
--

LOCK TABLES `relais` WRITE;
/*!40000 ALTER TABLE `relais` DISABLE KEYS */;
INSERT INTO `relais` VALUES (1,NULL,1,'LaPoste',NULL,NULL,NULL);
/*!40000 ALTER TABLE `relais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stockage`
--

LOCK TABLES `stockage` WRITE;
/*!40000 ALTER TABLE `stockage` DISABLE KEYS */;
INSERT INTO `stockage` VALUES (1,1,1,1,'2021-12-12 21:52:58',NULL),(2,2,4,1,'2021-12-12 21:52:58',NULL);
/*!40000 ALTER TABLE `stockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unite`
--

LOCK TABLES `unite` WRITE;
/*!40000 ALTER TABLE `unite` DISABLE KEYS */;
INSERT INTO `unite` VALUES (1,'g'),(2,'kg'),(3,'ml'),(4,'cl'),(5,'l'),(6,'pièce'),(7,'botte'),(8,'cagette');
/*!40000 ALTER TABLE `unite` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-23 16:04:17
