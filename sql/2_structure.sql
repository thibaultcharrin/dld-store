/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  17/12/2021 14:40:56                      */
/*==============================================================*/


drop table if exists accessibilite;

drop table if exists achat;

drop table if exists adherent;

drop table if exists ca_mesurer;

drop table if exists carnet;

drop table if exists cat;

drop table if exists cat2;

drop table if exists cat3;

drop table if exists conservation;

drop table if exists dimension;

drop table if exists distance;

drop table if exists don;

drop table if exists horaire;

drop table if exists indisponibilite;

drop table if exists jour;

drop table if exists localisation;

drop table if exists partenaire;

drop table if exists produit;

drop table if exists relais;

drop table if exists stockage;

drop table if exists unite;

/*==============================================================*/
/* Table : accessibilite                                        */
/*==============================================================*/
create table accessibilite
(
   id                   int not null auto_increment,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : achat                                                */
/*==============================================================*/
create table achat
(
   id                   int not null auto_increment,
   adherent_id          int not null,
   carnet_id            int not null,
   date                 datetime,
   qty                  int,
   primary key (id)
);

/*==============================================================*/
/* Table : adherent                                             */
/*==============================================================*/
create table adherent
(
   id                   int not null auto_increment,
   localisation_id      int not null,
   firstname            varchar(254),
   lastname             varchar(254),
   dob                  datetime,
   email                varchar(254),
   pw                   varchar(254),
   street_nb            int,
   street_name          varchar(254),
   comp_adr             varchar(254),
   tel                  varchar(254),
   create_date          datetime,
   confirm_date         datetime,
   deactivate_date      datetime,
   activation_code      int,
   primary key (id)
);

/*==============================================================*/
/* Table : ca_mesurer                                           */
/*==============================================================*/
create table ca_mesurer
(
   product_id           int not null,
   unit_id              int not null,
   primary key (product_id, unit_id)
);

/*==============================================================*/
/* Table : carnet                                               */
/*==============================================================*/
create table carnet
(
   id                   int not null auto_increment,
   name                 varchar(254),
   nb_cred              int,
   price                float,
   primary key (id)
);

/*==============================================================*/
/* Table : cat                                                  */
/*==============================================================*/
create table cat
(
   id                   int not null auto_increment,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : cat2                                                 */
/*==============================================================*/
create table cat2
(
   id                   int not null auto_increment,
   cat_id               int not null,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : cat3                                                 */
/*==============================================================*/
create table cat3
(
   id                   int not null auto_increment,
   cat2_id              int not null,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : conservation                                         */
/*==============================================================*/
create table conservation
(
   id                   int not null auto_increment,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : dimension                                            */
/*==============================================================*/
create table dimension
(
   id                   int not null auto_increment,
   stockage_id          int not null,
   name                 varchar(254),
   height               int,
   width                int,
   depth                int,
   primary key (id)
);

/*==============================================================*/
/* Table : distance                                             */
/*==============================================================*/
create table distance
(
   id                   int not null,
   localisation_id      int not null,
   avg_distance         int,
   primary key (id, localisation_id)
);

/*==============================================================*/
/* Table : don                                                  */
/*==============================================================*/
create table don
(
   id                   int not null auto_increment,
   beneficiary_id       int,
   produit_id           int not null,
   donor_id             int not null,
   stockage_id          int,
   unite_id             int not null,
   conservation_id      int not null,
   localisation_id      int,
   qty                  float,
   dlc                  datetime,
   ddm                  datetime,
   create_date          datetime,
   reserve_date         datetime,
   cancel_date          datetime,
   picture              varchar(254),
   alt_street_nb        int,
   alt_street_name      varchar(254),
   alt_comp_adr         int,
   available_collect    datetime,
   receive              datetime,
   no_receive           datetime,
   no_handover          datetime,
   handover             datetime,
   final_date           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table : horaire                                              */
/*==============================================================*/
create table horaire
(
   id                   int not null auto_increment,
   jour_id              int,
   don_id               int not null,
   stockage_id          int,
   morning_start        Time,
   morning_end          Time,
   afternoon_start      Time,
   afternoon_end        Time,
   primary key (id)
);

/*==============================================================*/
/* Table : indisponibilite                                      */
/*==============================================================*/
create table indisponibilite
(
   id                   int not null auto_increment,
   adherent_id          int not null,
   stockage_id          int not null,
   start                datetime,
   end                  datetime,
   register_date        datetime,
   cancel_date          datetime,
   primary key (id)
);

/*==============================================================*/
/* Table : jour                                                 */
/*==============================================================*/
create table jour
(
   id                   int not null auto_increment,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : localisation                                         */
/*==============================================================*/
create table localisation
(
   id                   int not null auto_increment,
   city                 varchar(254) not null,
   postcode             varchar(254) not null,
   primary key (id)
);

/*==============================================================*/
/* Table : partenaire                                           */
/*==============================================================*/
create table partenaire
(
   id                   int not null auto_increment,
   localisation_id      int not null,
   name                 varchar(254),
   email                varchar(254),
   pw                   varchar(254),
   tel                  varchar(254),
   street_nb            int,
   street_name          varchar(254),
   comp_adr             varchar(254),
   register_date        datetime,
   validate_date        datetime,
   deactivate_date      datetime,
   activation_code      int,
   primary key (id)
);

/*==============================================================*/
/* Table : produit                                              */
/*==============================================================*/
create table produit
(
   id                   int not null auto_increment,
   cat3_id              int,
   conservation_id      int not null,
   name                 varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : relais                                               */
/*==============================================================*/
create table relais
(
   id                   int not null auto_increment,
   localisation_id      int,
   partenaire_id        int not null,
   name                 varchar(254),
   street_nb            int,
   street_name          varchar(254),
   comp_adr             varchar(254),
   primary key (id)
);

/*==============================================================*/
/* Table : stockage                                             */
/*==============================================================*/
create table stockage
(
   id                   int not null auto_increment,
   accessibilite_id     int not null,
   conservation_id      int not null,
   relais_id            int not null,
   create_space         datetime,
   remove_space         datetime,
   primary key (id)
);

/*==============================================================*/
/* Table : unite                                                */
/*==============================================================*/
create table unite
(
   id                   int not null auto_increment,
   name                 varchar(254) not null,
   primary key (id)
);

alter table achat add constraint FK_Association_Carnet foreign key (carnet_id)
      references carnet (id) on delete restrict on update restrict;

alter table achat add constraint FK_Association_Adherent foreign key (adherent_id)
      references adherent (id) on delete restrict on update restrict;

alter table adherent add constraint FK_Association_3 foreign key (localisation_id)
      references localisation (id) on delete restrict on update restrict;

alter table ca_mesurer add constraint FK_mesurer_produit foreign key (product_id)
      references produit (id) on delete restrict on update restrict;

alter table ca_mesurer add constraint FK_mesurer_unite foreign key (unit_id)
      references unite (id) on delete restrict on update restrict;

alter table cat2 add constraint FK_Association_17 foreign key (cat_id)
      references cat (id) on delete restrict on update restrict;

alter table cat3 add constraint FK_Association_29 foreign key (cat2_id)
      references cat2 (id) on delete restrict on update restrict;

alter table dimension add constraint FK_Association_23 foreign key (stockage_id)
      references stockage (id) on delete restrict on update restrict;

alter table distance add constraint FK_Association_32 foreign key (id)
      references localisation (id) on delete restrict on update restrict;

alter table distance add constraint FK_Association_33 foreign key (localisation_id)
      references localisation (id) on delete restrict on update restrict;

alter table don add constraint FK_Association_11 foreign key (stockage_id)
      references stockage (id) on delete restrict on update restrict;

alter table don add constraint FK_Association_19 foreign key (conservation_id)
      references conservation (id) on delete restrict on update restrict;

alter table don add constraint FK_Association_31 foreign key (unite_id)
      references unite (id) on delete restrict on update restrict;

alter table don add constraint FK_Association_5 foreign key (localisation_id)
      references localisation (id) on delete restrict on update restrict;

alter table don add constraint FK_Association_7 foreign key (produit_id)
      references produit (id) on delete restrict on update restrict;

alter table don add constraint FK_beneficiary foreign key (beneficiary_id)
      references adherent (id) on delete restrict on update restrict;

alter table don add constraint FK_donor foreign key (donor_id)
      references adherent (id) on delete restrict on update restrict;

alter table horaire add constraint FK_Association_21 foreign key (jour_id)
      references jour (id) on delete restrict on update restrict;

alter table horaire add constraint FK_Association_24 foreign key (stockage_id)
      references stockage (id) on delete restrict on update restrict;

alter table horaire add constraint FK_Association_26 foreign key (don_id)
      references don (id) on delete restrict on update restrict;

alter table indisponibilite add constraint FK_Association_27 foreign key (adherent_id)
      references adherent (id) on delete restrict on update restrict;

alter table indisponibilite add constraint FK_Association_28 foreign key (stockage_id)
      references stockage (id) on delete restrict on update restrict;

alter table partenaire add constraint FK_Association_4 foreign key (localisation_id)
      references localisation (id) on delete restrict on update restrict;

alter table produit add constraint FK_Association_20 foreign key (conservation_id)
      references conservation (id) on delete restrict on update restrict;

alter table produit add constraint FK_Association_30 foreign key (cat3_id)
      references cat3 (id) on delete restrict on update restrict;

alter table relais add constraint FK_Association_12 foreign key (partenaire_id)
      references partenaire (id) on delete restrict on update restrict;

alter table relais add constraint FK_Association_6 foreign key (localisation_id)
      references localisation (id) on delete restrict on update restrict;

alter table stockage add constraint FK_Association_13 foreign key (relais_id)
      references relais (id) on delete restrict on update restrict;

alter table stockage add constraint FK_Association_18 foreign key (conservation_id)
      references conservation (id) on delete restrict on update restrict;

alter table stockage add constraint FK_Association_25 foreign key (accessibilite_id)
      references accessibilite (id) on delete restrict on update restrict;

